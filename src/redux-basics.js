const redux = require('redux'); //node js syntxe
const createStore = redux.createStore;

const initialState = {
    counter : 0
}
//reducer
//le current state prend la valeur initiale s il est undefined
const rootReducer = (state = initialState, action)=> { 
    if (action.type ==='INC_COUNTER'){
        return { 
//jamais ne modifier l'original state, faire tjs une copie
            ...state,
            counter : state.counter +1
        }
    }
    if (action.type ==='ADD_COUNTER'){
        return { 
            ...state,
            counter : state.counter +action.value
        }
    }
    return state
};
// store
const store = createStore(rootReducer);
//console.log(store.getState());

//subscription // executé apres chaque action qui modifie le state
store.subscribe(()=> {
    console.log('subscription', store.getState())
});
//dispatching action
//type attribt obligatoire + sa valeur majuscule convention
store.dispatch({type: 'INC_COUNTER'});
store.dispatch({type: 'ADD_COUNTER', value : 10});


